#!/bin/bash

# The instructions to install https://git.sr.ht/~anteater/vgmms did not work
# I've modified them a little bit and scripted it to save the work next time
# I have no idea what I am doing is this even how you linux phone omgwtfhelp
# 2020 silver was here

set -e

modprobe zram num_devices=1
zramctl /dev/zram0 -s 1G
mkswap /dev/zram0
swapon /dev/zram0

apt-get install git build-essential || apk add git gcc autoconf automake libtool make libc-dev glib-dev dbus-dev || pacman -S --needed git gcc base-devel python-dbus

[ -d ell ] || git clone https://github.com/bryanperris/ell.git

[ -d ofono ] || git clone https://git.sr.ht/~anteater/ofono

cd ofono
./bootstrap 
./configure --prefix=/usr \
                --mandir=/usr/share/man \
                --sysconfdir=/etc \
                --localstatedir=/var
sed -i 's/Werror/Wno-error/' Makefile
make && make install

cd ..

[ -d ofonoctl ] || git clone https://git.sr.ht/~martijnbraam/ofonoctl
cd ofonoctl
python setup.py build
python setup.py install --prefix=/usr

cd ..

[ -d mmsd ] || git clone https://git.sr.ht/~anteater/mmsd
cd mmsd
./bootstrap
./configure --prefix=/usr --sysconfdir=/etc --sbindir=/usr/bin
sed -i 's/Werror/Wno-error/' Makefile
make && make install

cd ..

apk add rust || apt-get install rust || pacman -S --needed rust || curl https://sh.rustup.rs -sSf | sh

[ -d vgmms ] || git clone https://git.sr.ht/~anteater/vgmms

cd vgmms

cargo build --release --jobs 1 # default = # cores uses too much ram

cd ..

ps aux | grep mmsd

ps aux | grep ofono

echo did it work?
